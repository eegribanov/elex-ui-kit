import React, { Component } from 'react'

export default class Wrapper extends Component {
  render() {
    return (
        <div className="el-ex-kit_wrapper">
            {this.props.children}
        </div >
    )
  }
}