import React from 'react';
import PropTypes from 'prop-types';
import Styled from 'rsg-components/Styled';
import logo from './lx-logo.svg';

const styles = ({ fontFamily, color }) => ({
	logo: {
		display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        margin: 0,
		fontFamily: fontFamily.base,
		fontSize: 18,
		fontWeight: 'normal',
		color: color.baseBackground
	},
	href: {
		cursor: 'pointer',
		width: '50%'
	},
	image: {
		cursor: 'inherit',
		width: '100%'
	},
});

export function LogoRenderer({ classes, children }) {
	return (
		<h1 className={classes.logo}>
			<a className={classes.href} href="/">
				<img className={classes.image} src={logo} alt="Electronic Express Logo" />
				{children}
			</a>
		</h1>
	);
}

LogoRenderer.propTypes = {
	classes: PropTypes.object.isRequired,
	children: PropTypes.node,
};

export default Styled(styles)(LogoRenderer);