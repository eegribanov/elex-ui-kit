import React from 'react';
import PropTypes from 'prop-types';
import Logo from 'rsg-components/Logo';
import Styled from 'rsg-components/Styled';
import Drawer from '@material-ui/core/Drawer';
import Ribbon from 'rsg-components/Ribbon';

const styles = ({ font, base, light, link, baseBackground, mq }) => ({
	root: {
		color: base,
		backgroundColor: baseBackground,
	},
	drawerPaper: {
		boxSizing: 'border-box',
		padding: 20,
		width: 250
	},
	header: {
		paddingBottom: '20px',
		borderBottom: '1px solid #efefef'
	},
	
	content: {
		maxWidth: '100%',
		padding: [[30, 45]],
		margin: [[0, 'auto']],
		marginLeft: 250,
		[mq.small]: {
			padding: 15,
		},
		display: 'block',
	},

	components: {
		overflow: 'auto', // To prevent the pane from growing out of the screen
	},
	footer: {
		display: 'block',
		color: light,
		fontFamily: font,
		fontSize: 12,
	},
});

export function StyleGuideRenderer({ classes, title, homepageUrl, children, toc }) {
	return (
		<div className={classes.root}>
			<main className={classes.content}>
				{children}
			</main>

			<Drawer
				className={classes.drawer}
				variant="permanent"
				classes={{ paper: classes.drawerPaper }}
				anchor="left"
			>
				<header className={classes.header}>
					<Logo />
				</header>
				{toc}
			</Drawer>

			<Ribbon />
		</div>
	);
}

StyleGuideRenderer.propTypes = {
	classes: PropTypes.object.isRequired,
	title: PropTypes.string.isRequired,
	homepageUrl: PropTypes.string.isRequired,
	children: PropTypes.node.isRequired,
	toc: PropTypes.node.isRequired,
};

export default Styled(styles)(StyleGuideRenderer);