import React from "react";
import { AppBar, Toolbar, IconButton, Tooltip } from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import ExitToAppTwoToneIcon from '@material-ui/icons/ExitToAppTwoTone';
import './TopBar.scss';

class TopBar extends React.Component 
{
  render() {
    return (
      <AppBar classes={{root: 'elex-top-bar'}}>
        <Toolbar>
          <Tooltip title="Меню">
            <IconButton edge="start" color="inherit" aria-label="menu">
                <MenuIcon />
            </IconButton>
          </Tooltip>

          <Tooltip title="Вход">
            <IconButton edge="end" color="inherit" aria-label="login">
              <ExitToAppTwoToneIcon />
            </IconButton>
          </Tooltip>
        </Toolbar>    
      </AppBar>
    )
  }
}

export default TopBar;