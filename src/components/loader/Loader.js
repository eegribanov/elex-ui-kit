import React from "react";
import PropTypes from "prop-types";
import Spinner from "./../spinner/Spinner";
import Backdrop from "@material-ui/core/Backdrop";
import Typography from '@material-ui/core/Typography';
import { Box, Container } from "@material-ui/core";
import './Loader.scss'

class Loader extends React.Component 
{
    static propTypes = {
        open: PropTypes.bool,
        label: PropTypes.string
    }
  
    static defaultProps = {
      open: true,
    }

    funnyLabel() {
        let labels = [
            "Не обижайтесь на людей",
            "Сделайте перерыв",
            "Улыбнитесь, вам идет",
            "Крутой сайт скоро появится",
            "Хэй, красавчик! Да, ты! Подожди и увидишь классный сайт"
        ]   
        let index = parseInt(Math.random() * 100 % labels.length)
        return labels[index]
    }

    
    render() {
        let label = this.props.label ? this.props.label : this.funnyLabel()
        return (
            <Backdrop open={this.props.open} classes={{root: 'elex-loader'}}>
                <Box>
                    <Container>
                        <Spinner />
                    </Container>
                    <Container>
                        <Typography variant='h4' classes={{root: 'elex-loader-label'}}>{label}</Typography>
                    </Container>
                </Box>
            </Backdrop>
        )
    }
}

export default Loader;