// styleguide.config.js
const path = require('path')

module.exports = {
  title: 'lx UI KIT',
  components: 'src/components/**/*.js',
  pagePerSection: true,

  template: {
    favicon: 'lx-logo.svg'
  },

  theme: {
		baseBackground: '#fdfdfc',
		link: '#274e75',
		linkHover: '#90a7bf',
		border: '#e0d2de',
		font: ['Helvetica', 'sans-serif'],
	},
  styleguideComponents: {
    LogoRenderer: path.join(__dirname, 'src/styleguide/Logo'),
    StyleGuideRenderer: path.join(__dirname, 'src/styleguide/StyleGuide'),
		SectionsRenderer: path.join(__dirname, 'src/styleguide/Sections'),
  }
}